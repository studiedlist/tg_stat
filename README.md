This repository contains:
- crate 'tg_stat', which have few useful functions and structures to parse Telegram export data
- crate 'message_extracter', which is interactive cli utility that allows to extract messages from selected users in selected chats
