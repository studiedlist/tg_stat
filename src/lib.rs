use std::fs;
use std::path::PathBuf;
use serde::Deserialize;
use serde_json::Map;
use fs::File;
use std::io::BufReader;

pub use serde::Serialize as Serialize;
pub use serde_json::Value as Value;

#[derive(Serialize, Deserialize)]
pub struct Data {
    about: Option<String>,
    pub chats: Chats,
}

#[derive(Serialize, Deserialize)]
pub struct Chats {
    about: Option<String>,
    pub list: Vec<Chat>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Chat {
    pub name: Option<String>,
    #[serde(alias = "type")] 
    pub chat_type: String,
    pub id: u64,
    pub messages: Vec<Message>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Message {

    #[serde(skip_serializing_if = "Option::is_none")]
    id: Option<u64>,
    #[serde(alias = "type")] 
    #[serde(skip_serializing_if = "Option::is_none")]
    message_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<String>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    edited: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    #[serde(default)]
    pub text: Option<serde_json::Value>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from_id: Option<String>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    file: Option<String>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    thumbnail: Option<String>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    mime_type: Option<String>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    width: Option<u32>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    height: Option<u32>,
    #[serde(default)] 
    #[serde(skip_serializing_if = "Option::is_none")]
    href: Option<String>
    
}

impl Message {
    
    pub fn from_text(text: &Value) -> Message {
        Message {
            text: Some(text.clone()),
            id: None,
            message_type: None,
            date: None,
            edited: None,
            from: None,
            from_id: None,
            file: None,
            thumbnail: None,
            mime_type: None,
            width: None,
            height: None,
            href: None,
        }
    }

}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Text {
    STRING(String),
    ARRAY(Vec<String>),
}

pub fn trim_data_fields<F>(data: &F, fields: Vec<String>) -> Result<Value, serde_json::Error>
where F: Serialize {
    let data = serde_json::to_value(data)?;
    let trim_fields = |data: Map<String, Value>| -> Map<String, Value> {
        data.into_iter()
        .filter(|(k, _)| {
            fields.contains(k)
        })
        .fold(Map::new(), |mut result, (k, v)| {
            result.insert(k, v);
            result
        })
    };
    Ok(match data {
        Value::Array(array) => {
            Value::Array(array.into_iter()
                .fold(vec![], |mut result, val| {
                    if let Value::Object(map) = val {
                        result.push(Value::Object(trim_fields(map)));
                    }
                    result
                }))
        },
        Value::Object(map) => Value::Object(trim_fields(map)),
        _ => panic!()
    })
}

pub fn get_fields<F: Serialize>(data: F) -> Result<Vec<String>, serde_json::Error> {
    let data = serde_json::to_value(data)?;
    let extract_keys = |map: &Map<String, Value>| -> Vec<String> {
        map.keys().fold(vec![], |mut result, key| {
            result.push(key.clone());
            result
        })
    };
    match data {
        Value::Array(array) => {
            if let Value::Object(map) = &array[0] {
                return Ok(extract_keys(map))
            }
            panic!()
        },
        Value::Object(map) => Ok(extract_keys(&map)),
        _ => panic!(),
    }
}

pub fn read_data(path: &PathBuf) -> Result<Data, serde_json::Error> {
    let reader = BufReader::new(File::open(path).unwrap());
    serde_json::from_reader(reader)
}

pub fn read_chat(path: &PathBuf) -> Result<Chat, serde_json::Error> {
    let reader = BufReader::new(File::open(path).unwrap());
    serde_json::from_reader(reader)
}

pub fn write<F: Serialize>(data: F, path: &PathBuf) -> Result<(), serde_json::Error> {
    let json = serde_json::to_string_pretty(&data)?;
    fs::write(path, json).unwrap();
    Ok(())
}
