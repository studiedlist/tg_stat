This crate is an interactive cli utility, which extracts Telegram export data  

Follow these steps to extract messages:
1. clone repository
1. build crate with `cargo b --release`
1. set up utility with your data using one of listed options
    - move data to utility
        1. place your export data to cloned repository's folder
        1. rename data file to `input.json`
        1. run utility with `cargo r --release`
    - pass data path to utility using -i flag
        1. run utility with `target/release/message_extracter -i %data_path%`
1. follow on-screen instructions
1. grab result at `output.json`

# Telegram export types
Crate support both single-chat export and multi-chat export.  
Channel export wasn't tested, and likely won't work for now
