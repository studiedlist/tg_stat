use read_input::prelude::*;
use std::collections::HashSet;
use regex::Regex;

pub fn get_range_input(min: usize, max: usize) -> HashSet<usize> {
    loop {
        let user_input = input::<String>()
            .add_test(move |input| check_range(input, min, max) || input == "a")
            .repeat_msg("Your input: ")
            .err("Please enter a valid range")
            .get();
        if user_input == "a" {
            let mut set = HashSet::new();
            for x in min..=max {
                set.insert(x);
            }
            return set
        }
        let range = parse_range(&user_input);
        if range.len() > 0 {
            return range
        }
        println!("Please enter a valid range");
    }
}

pub fn check_range(range: &str, min: usize, max: usize) -> bool {
    let digit_regex = Regex::new(r"^\d+$").unwrap();
    if digit_regex.is_match(range) {
        let num : usize = range.parse().unwrap();
        return num >= min && num <= max
    }
    let regex = Regex::new(r"((\d+[,-]\d+)+|^\d+$)").unwrap();
    let rev_regex = Regex::new(r"([^\d,-]+|[,-]$|^[,-])").unwrap();
    if !regex.is_match(range) || rev_regex.is_match(range) {
        return false
    }
    let partial_regex = Regex::new(r"\d+[,-]\d+").unwrap();
    partial_regex.find_iter(range).fold(true, |mut result, elem| {
        let elem = elem.as_str();
        elem.rsplit(|c| c == '-' || c == ',').for_each(|split| {
            let num : usize = split.parse().unwrap();
            if num < min || num > max {
                result = false;
            }
        });
        result
    })
}

pub fn parse_range(range: &str) -> HashSet<usize> {
    let mut result = HashSet::new();
    let single_num_regex = Regex::new(r"^\d+$").unwrap();
    if single_num_regex.is_match(range) {
        result.insert(range.parse().unwrap());
        return result
    }
    let partial_regex = Regex::new(r"(\d+)?[,-]\d+").unwrap();
    let range_regex = Regex::new(r"(\d+)?-\d+").unwrap();
    let add_regex = Regex::new(r"^?(\d+)?,\d+?").unwrap();
    let mut last : usize = 0;
    partial_regex.find_iter(range).fold(result, |mut result, elem| {
        let elem = elem.as_str();
        if range_regex.is_match(elem) {
            let mut split = elem.rsplit("-").fold(vec![], |mut result, elem| {
                if let Ok(elem) = elem.parse() {
                    result.push(elem);
                }
                result
            });
            split.sort();
            if split.len() == 1 {
                split.insert(0, last);
            }
            for x in split[0]..=split[1] {
                result.insert(x);
            }
            last = split[split.len() - 1];
        }
        if add_regex.is_match(elem) {
            let split = elem.rsplit(",").fold(vec![], |mut result, elem| {
                if let Ok(elem) = elem.parse() {
                    result.push(elem);
                }
                result
            });
            for elem in &split {
                result.insert(*elem);
            }
            last = split[split.len() - 1];
        } 
        result
    })
}

pub fn range_help() -> String {
    let mut result = String::new();
    result.push_str("Range syntax:\n");
    result.push_str(" - 'x-y' means all entries from x to y\n");
    result.push_str(" - 'x,y' means entry x and entry y\n");
    result.push_str(" ^ Feel free to mix these expressions like 'x-y,z-w' or 'x-y,z,w'\n");
    result.push_str(" - 'a'   means all entries\n");
    result
}
