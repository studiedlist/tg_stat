#[allow(unused_imports)]
use log::{debug, error, log_enabled, info, Level};
use clap::Parser;
use std::fs::File;
use std::path::PathBuf;
use read_input::prelude::*;
use tg_stat::{Message, Chat, Data};
use std::collections::{HashMap, HashSet};
use crate::range::*;

#[cfg(test)]
mod test;
mod range;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// File to read from
    #[clap(short, long)]
    input_file: Option<String>,

    /// File to write to
    #[clap(short, long)]
    output_file: Option<String>,
}

enum ExtractMode {
    ById(Vec<String>),
    Manual,
}

const DEFAULT_FILE_PATH : &str = "input.json";
const DEFAULT_OUTPUT_PATH : &str = "output.json";

fn main() {
    env_logger::init();
    let args = Args::parse();
    let output_path = match args.output_file {
        Some(ref path) => path.clone(),
        None => {
            info!("Output file parameter is not present, defaulting to \'{}\'", DEFAULT_OUTPUT_PATH);
            String::from(DEFAULT_OUTPUT_PATH)
        },
    };

    let path = match args.input_file {
        Some(path) => path,
        None => {
            info!("Input file parameter is not present, defaulting to \'{}\'", DEFAULT_FILE_PATH);
            String::from(DEFAULT_FILE_PATH)
        },
    };
    match File::open(&path) {
        Ok(file) => file,
        Err(err) => {
            error!("Cannot open file \'{}\', error is: {}", path, err);
            return;
        }
    };
    let path = PathBuf::from(path);
    let output_path = PathBuf::from(output_path);
    if path == output_path {
        panic!("Output and input paths cannot be the same");
    }
    interactive_extract(&path, &output_path);
}

fn interactive_extract(input_path: &PathBuf, output_path: &PathBuf) {
    println!("Note:");
    println!(" - program will not change your input data, so feel free to experiment with it");
    println!(" - you can safely quit program anytime");
    println!("");
    println!("If you find any bug, typo, or missing feature, please create issue at https://gitlab.com/studiedlist/tg_stat/");
    println!("");
    let options = vec!["One chat", "Multiple chats", "Select automatically (might be slow)"];
    let user_input = list_choice("Select your data type", &options);
    println!("Wait a while, parsing might take some time..");
    let print_error_message = |msg: &str| {
        error!("Cannot parse data, error is:\n{}", msg);
        println!("Something went wrong..");
    };
    let data = match user_input {
        1 => {
            match tg_stat::read_chat(&input_path) {
                Ok(chat) => extract_from_chat_interactive(&chat),
                Err(err) => {
                    print_error_message(&format!("{}", err));
                    return
                }
            }
        },
        2 => {
            match tg_stat::read_data(&input_path) {
                Ok(data) => extract_from_data_interactive(&data),
                Err(err) => {
                    print_error_message(&format!("{}", err));
                    return
                }
            }
        },
        3 => {
            match tg_stat::read_chat(&input_path) {
                Ok(chat) => extract_from_chat_interactive(&chat),
                Err(_) => {
                    match tg_stat::read_data(&input_path) {
                        Ok(data) => extract_from_data_interactive(&data),
                        Err(err) => {
                            print_error_message(&format!("{}", err));
                            return
                        }
                    }
                }
            }
        },
        _ => panic!(),
    };
    let print_err = || println!("No messages have been extracted!");
    match data {
        None => {
            print_err();
            return
        },
        Some(x) if x.len() == 0 => {
            print_err();
            return
        },
        _ => ()
    };
    let msg = "Do you want to include all messages fields, or choice which to keep?";
    let options = vec!["Include all", "Choice which to keep"];
    let mode = list_choice(msg, &options);
    fn save_data<F: tg_stat::Serialize>(data: F, path: &PathBuf) {
        if let Err(err) = tg_stat::write(data, path) {
            error!("Cannot write data, error is:\n{}", err);
            return
        };
    }
    match mode {
        1 => save_data(data, &output_path),
        2 => {
            let fields = match tg_stat::get_fields(&data) {
                Ok(fields) => fields,
                Err(err) => {
                    error!("Cannot get data fields\n{}", err);
                    save_data(data, &output_path);
                    return
                },
            };
            println!("Please select fields to keep:");
            println!("{}", range_help());
            fields.iter().enumerate().for_each(|(i, field)| {
                println!("{}. {}", i + 1, field);
            });
            let input : HashSet<usize> = get_range_input(1, fields.len())
                .iter()
                .map(|e| e - 1)
                .collect();
            let fields : Vec<String> = fields.into_iter()
                .enumerate()
                .filter(|(i, _)| input.contains(i))
                .fold(vec![], |mut result, (_, v)| {
                    result.push(v);
                    result
                });
            let data = match tg_stat::trim_data_fields(&data, fields) {
                Ok(data) => data,
                Err(err) => {
                    error!("Cannot format data, error is:\n{}", err);
                    save_data(data, &output_path);
                    return
                }
            };
            save_data(data, &output_path);
        },
        _ => panic!()
    }
}

fn extract_from_data_interactive(data: &Data) -> Option<Vec<Message>> {
    let options = vec!["Manually choose users in chats", "Enter user id(s) to extract from"];
    let mode = list_choice("How do you want to extract data?", &options);
    let mut mode = match mode {
        1 => ExtractMode::Manual,
        2 => ExtractMode::ById(vec![]),
        _ => panic!(),
    };
    if let ExtractMode::ById(ids) = &mut mode {
        println!("Enter user id");
        let user_input : String = input().get();
        ids.push(user_input);
        loop {
            println!("Enter user id or press enter to start parsing");
            let user_input : String = input().get();
            if user_input == "" {
                break;
            }
            ids.push(user_input);
        }
    }
    extract_from_data(&data, &mode)
}

fn extract_from_data(data: &Data, mode: &ExtractMode) -> Option<Vec<Message>> {
    let chats = parse_chats(&data);
    match chats.len() {
        0 => { 
            println!("No chats in data");
            None
        },
        1 => extract_from_chat_interactive(&data.chats.list[0]),
        _ => {
            let mut vec = chats.iter().fold(vec![], |mut vec, (k, v)| {
                vec.push((*k as usize, v));
                vec
            });
            vec.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
            vec.iter()
                .enumerate()
                .for_each(|(i, (k, v))| println!("{}. {}\t{}", i+1, k, v));
            println!("Please select chats to extract messages from");
            println!("{}", range_help());
            let range = get_range_input(1, vec.len());
            let mut range : Vec<usize> = range.into_iter().map(|elem| {
                vec[elem - 1].0
            }).collect();
            range.sort();
            let extract; 
            match mode {
                ExtractMode::Manual => {
                    extract = Box::new(|chat: &Chat| -> Option<Vec<Message>> {
                        extract_from_chat_interactive(&chat)
                    }) as Box<dyn Fn(&Chat) -> Option<Vec<Message>>>;
                },
                ExtractMode::ById(ids) => {
                    extract = Box::new(move |chat: &Chat| -> Option<Vec<Message>> {
                        extract_from_chat(&chat, &ids)
                    }) as Box<dyn Fn(&Chat) -> Option<Vec<Message>>>;
                }
            }
            let result = data.chats.list.iter()
                .filter(|chat| range.contains(&(chat.id as usize)))
                .fold(vec![], |mut result, chat| {
                    if let Some(msgs) = &mut extract(&chat) {
                        result.append(msgs)
                    }
                    result
                });
            Some(result)
        }
    }
}

fn extract_from_chat_interactive(chat: &Chat) -> Option<Vec<Message>> {
    let users = parse_users(&chat);
    match users.len() {
        0 => None,
        1 => Some(extract_messages(&chat, &users.keys().next().unwrap())),
        _ => {
            let mut vec = users.iter().fold(vec![], |mut vec, (k, v)| {
                vec.push((k, v));
                vec
            });
            vec.sort_by(|a, b| a.1.0.partial_cmp(&b.1.0).unwrap());
            vec.iter()
                .enumerate()
                .for_each(|(i, (k, v))| {
                    println!("{}. {}\t({} messages)\t {}", i+1, k, v.0, v.1);
                });
            println!("Please select users to extract messages from:");
            println!("{}", range_help());
            let range = get_range_input(1, vec.len());
            let range : HashSet<&String> = range.iter().map(|elem| {
                vec[elem - 1].0
            }).collect();
            Some(users.iter().fold(vec![], |mut result, user| {
                if range.contains(user.0) {
                    result.append(&mut extract_messages(&chat, user.0));
                }
                result
            }))
        }
    }
}

fn list_choice(msg: &str, options: &Vec<&str>) -> usize {
    println!("{}", msg);
    options.iter().enumerate().for_each(|(i, o)| {
        println!("{}. {}", i+1, o);
    });
    let max = options.len();
    input()
        .add_test(move |num| *num <= max && *num > 0)
        .msg("Your input: ")
        .err("Please enter a valid number")
        .get()
}

fn extract_from_chat(chat: &Chat, ids: &Vec<String>) -> Option<Vec<Message>> {
    let users = parse_users(&chat);
    if users.len() == 0 {
        return None;
    }
    Some(users.keys().fold(vec![], |mut result, key| {
        if ids.contains(key) {
            result.append(&mut extract_messages(&chat, key));
        }
        result
    }))
}

fn parse_chats(data: &Data) -> HashMap<u64, String> {
    let chats = HashMap::new();
    data.chats.list.iter()
        .fold(chats, |mut chats, chat| {
            if let (id, Some(name)) = (&chat.id, &chat.name) {
                if let None = chats.get_mut(id) {
                    chats.insert(*id, name.clone());
                }
            }
            chats
        })
}

fn parse_users(chat: &Chat) -> HashMap<String, (usize, String)> {
    let ids = HashMap::new();
    chat.messages.iter()
        .fold(ids, |mut ids, msg| {
            if let (Some(id), Some(name)) = (&msg.from_id, &msg.from) {
                match ids.get_mut(id) {
                    Some(val) => val.0 += 1,
                    None => {
                        ids.insert(id.clone(), (1, name.clone()));
                    },
                };
            }
            ids
        })
}

fn extract_messages(chat: &Chat, user_id: &String) -> Vec<Message> {
    chat.messages.iter()
        .filter(|msg| {
            if let Some(tg_stat::Value::String(text)) = &msg.text {
                if text.len() < 3 {
                    return false;
                }
                match &msg.from_id {
                    Some(id) => return id == user_id,
                    None => return false
                }
            };
            false
        })
        .fold(vec![], |mut messages, msg| {
            messages.push(msg.clone());
            messages
        })
}
