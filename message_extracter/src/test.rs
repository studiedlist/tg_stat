
#[cfg(test)]
mod test {

    use std::collections::HashSet;
    use crate::{parse_range, check_range};

    #[test]
    fn checks_range() {
        assert!(check_range("10", 0, 20));
        assert!(check_range("1-10", 0, 20));
        assert!(!check_range("1-30", 0, 20));
        assert!(check_range("1,10-21", 0, 20));
        assert!(!check_range("1/3", 0, 10));
        assert!(!check_range("1-3,abc", 0, 10));
        assert!(!check_range("1-3,", 0, 10));
        assert!(!check_range(",1-3,", 0, 10));
        assert!(!check_range("-1-3", 0, 10));
    }

    #[test]
    fn parses_range() {
        let mut set = HashSet::new();
        set.insert(1);
        assert!(parse_range("1") == set);
        let mut set = HashSet::new();
        set.insert(1);
        set.insert(2);
        set.insert(3);
        set.insert(5);
        assert!(parse_range("1-3,5") == set);
        let mut set = HashSet::new();
        set.insert(1);
        set.insert(2);
        assert!(parse_range("1,2") == set);
        let mut set = HashSet::new();
        set.insert(1);
        set.insert(2);
        set.insert(3);
        assert!(parse_range("1-3") == set);
        let mut set = HashSet::new();
        set.insert(1);
        set.insert(2);
        set.insert(3);
        set.insert(5);
        set.insert(6);
        set.insert(7);
        assert!(parse_range("1-3,5-7") == set);
        let mut set = HashSet::new();
        set.insert(1);
        set.insert(2);
        set.insert(3);
        set.insert(5);
        set.insert(6);
        set.insert(7);
        set.insert(9);
        assert!(parse_range("1-3,5-7,9") == set);
    }


}
